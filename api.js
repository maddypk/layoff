(function($) {
  console.log("   its working")
      "use strict";
  if ($("#contact-form").length) {
          $("#contact-form").validate({
              rules: {
                  name: {
                      required: true,
                      minlength: 2
                  },
  
                  email: "required",
  
                  subject: "required",
                  company: "required",
                  query: "required",
  
              },
  
              messages: {
                  name: "Please enter your name",
                  email: "Please enter your email address",
                  contact : "Please enter your contact",
                  subject: "Please enter subject",
                  company : "Please enter company name",
              },
              
              submitHandler: function (form) {
                 let data = {
                      Name : $('#name').val(),
                      Email : $('#email').val(),
                      Message : $('#note').val(),
                      Query : $('#query').val(),
                      Phone : $('#phone').val(),
                      Company : $('#company').val(),
                  }
                  console.log("data send ", data);
                  $.ajax({
                      type: "POST",
                      // url: "http://68.183.90.78:3013/techmatter",
                      data: data,
                      success: function (result) {
                          $( "#loader").hide();
                          $( "#success").slideDown( "slow" );
                          setTimeout(function() {
                          $( "#success").slideUp( "slow" );
                          }, 3000);
                          console.log(result)
                          form.reset();
                      },
                      error: function() {
                          $( "#loader").hide();
                          $( "#error").slideDown( "slow" );
                          setTimeout(function() {
                          $( "#error").slideUp( "slow" );
                          }, 3000);
                      }
                  });
                  return false; // required to block normal submit since you used ajax
              }
  
          });
      }
  })(window.jQuery);